#include <igloo/igloo_alt.h>
using namespace igloo;

#include "board.h"

Describe(A_Board) {

    It(Should_spawn_an_empty_board_if_empty) {
        Board board;

        Assert::That(board.spawn().live_cells(), HasLength(0));
    }

    It(Should_be_empty_if_seeded_empty) {
        Board board;
        board.seed(std::list<Cell>());

        Assert::That(board.spawn().live_cells(), HasLength(0));
    }

    It(Should_handle_the_blinker_formation) {
        Board board;
        std::list<Cell> seed;
        seed.push_back(Cell(0,1));
        seed.push_back(Cell(1,1));
        seed.push_back(Cell(2,1));
        
        board.seed(seed);
        
        Board next = board.spawn();

        Assert::That(next.live_cells(), HasLength(3));
        Assert::That(next.live_cells(), Contains(Cell(1,1)));  
        Assert::That(next.live_cells(), Contains(Cell(1,0)));  
        Assert::That(next.live_cells(), Contains(Cell(1,2)));  
    }
};