/* 
 * File:   board.h
 * Author: nano
 *
 * Created on 26 de julio de 2013, 20:39
 */

#ifndef BOARD_H
#define	BOARD_H

#include "cell.h"

class Board {
    std::list<Cell> cells_;

public:

    Board spawn() const {
        Board board;
        std::list<Cell> next_gen;

        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                Cell cell(x, y);
                if (cell.will_survive_in(cells_)) {
                    next_gen.push_back(cell);
                }
            }
        }
        board.seed(next_gen);
        return board;
    }

    void seed(const std::list<Cell> cells) {
        cells_ = cells;
    }

    const std::list<Cell>& live_cells() const {
        return cells_;
    }
};

#endif	/* BOARD_H */

