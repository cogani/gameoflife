/* 
 * File:   cell.h
 * Author: nano
 *
 * Created on 26 de julio de 2013, 17:18
 */

#ifndef CELL_H
#define	CELL_H

class Cell{
    int x_;
    int y_;
    
public:
    Cell(int x, int y): x_(x), y_(y){
        
    }
    int get_x() const{
        return x_;
    }
    
    int get_y() const{
        return y_;
    }
    
    bool operator ==(const Cell& other) const{
        return x_ == other.x_ && y_ == other.y_;
    }
    
    bool operator !=(const Cell& other) const{
        return !(*this == other);
    }
       
    bool is_neighbor_to(const Cell& other) const{
        return abs(x_ - other.x_) <=1 && 
                abs(y_ - other.y_) <=1 &&
                 *this != other;
    }
    
    bool will_survive_in(const std::list<Cell>& environment) const{                
        int neighbors = count_neighbors(environment);
        
        if(exists_in(environment)){
            return neighbors==2 || neighbors==3;
        }
        
        return neighbors==3;
        
    }    
    
    bool exists_in(const std::list<Cell>& environment) const{
        return std::find(environment.begin(), environment.end(),
                *this) != environment.end();
    }
    
    int count_neighbors(const std::list<Cell>& others) const{
        int count = 0;
        std::list<Cell>::const_iterator it;
        for(it=others.begin(); it != others.end();it++){
            if(is_neighbor_to(*it)){
                count++;
            }              
        }
        return count;
    }
};
#endif	/* CELL_H */

