#include <igloo/igloo_alt.h>
using namespace igloo;


#include "cell.h"

Describe(A_Cell) {

    It(Should_have_coordinates) {
        Cell cell(3, 4);
        Assert::That(cell.get_x(), Equals(3));
        Assert::That(cell.get_y(), Equals(4));
    }

    It(Can_detect_if_its_a_neighbor) {
        Cell cell(1, 5);
        Cell neighbor(2, 4);

        Assert::That(cell.is_neighbor_to(neighbor), IsTrue());
    }

    It(Can_detect_that_its_not_a_neighbor) {
        Cell cell(4, 7);
        Cell no_neighbor(2, 12);

        Assert::That(cell.is_neighbor_to(no_neighbor), IsFalse());
    }

    It(Is_not_a_neighbor_to_itself) {
        Cell cell2(34, 65);
        Assert::That(cell2.is_neighbor_to(cell2), IsFalse());
    }

    It(Detects_when_it_has_no_neighbors) {
        std::list<Cell> no_neighbors;
        no_neighbors.push_back(Cell(2, 3));

        Cell cell2(5, 5);
        Assert::That(cell2.count_neighbors(no_neighbors), Equals(0));
    }

    It(Can_count_neighbors_from_a_list_of_cells) {
        std::list<Cell> neighbors;
        neighbors.push_back(Cell(2, 3));
        neighbors.push_back(Cell(3, 3));
        neighbors.push_back(Cell(12, 3));

        Cell cell(2, 2);
        Assert::That(cell.count_neighbors(neighbors), Equals(2));
    }

    It(Survives_if_it_has_two_neigbors) {
        std::list<Cell> environment;
        environment.push_back(Cell(1, 1));
        environment.push_back(Cell(0, 2));
        environment.push_back(Cell(2, 2));

        Cell cell(1, 1);
        Assert::That(cell.will_survive_in(environment), IsTrue());
    }

    It(Dies_if_it_alone) {
        std::list<Cell> environment;

        Cell cell(3, 4);
        Assert::That(cell.will_survive_in(environment), IsFalse());
    }

    It(Dies_if_has_four_neighbors) {
        std::list<Cell> environment;
        environment.push_back(Cell(0, 0));
        environment.push_back(Cell(0, 1));
        environment.push_back(Cell(1, 1));
        environment.push_back(Cell(0, 2));
        environment.push_back(Cell(1, 0));

        Cell cell(0, 1);
        Assert::That(cell.will_survive_in(environment), IsFalse());
    }

    It(Survives_if_it_has_three_neighbors) {
        std::list<Cell> environment;
        environment.push_back(Cell(0, 0));
        environment.push_back(Cell(0, 1));
        environment.push_back(Cell(1, 1));
        environment.push_back(Cell(0, 2));

        Cell cell(0, 1);
        Assert::That(cell.will_survive_in(environment), IsTrue());
    }

    It(Will_spawn_if_it_has_three_neighbors) {
        std::list<Cell> environment;
        environment.push_back(Cell(12, 45));
        environment.push_back(Cell(13, 46));
        environment.push_back(Cell(12, 47));

        Cell cell(12, 46);
        Assert::That(cell.will_survive_in(environment), IsTrue());
    }

    It(Will_spawn_if_it_has_two_neighbors) {
        std::list<Cell> environment;
        environment.push_back(Cell(0, 0));
        environment.push_back(Cell(0, 2));

        Cell cell(0, 1);
        Assert::That(cell.will_survive_in(environment), IsFalse());
    }


};